//
//  storage_mgr.c
//
//
//  Created by Gates_ice on 1/21/15.
//
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "storage_mgr.h"

#define WRITE_OPERATION(__statement__) if (-1 == __statement__) { return RC_WRITE_FAILED; }
#define CHECK_FILE_HANDLE(fHandle) if (!fHandle) { return RC_FILE_HANDLE_NOT_INIT; }
#define CHECK_PTR(__ptr__, __err__) if (!__ptr__) { return __err__; }

/**
 * initStorageManager
 **/
void initStorageManager (void) {
    return;
}

/**
 * createPageFile
 *     Create a new page file fileName. The initial file size should be one page. This method
 *     should fill this single page with '\0' bytes.
 *   parameters:
 *     fileName: the filename of the page file.
 *   return:
 *     The status of the file creation.
 *     If the file failed to create, return RC_FILE_HANDLE_NOT_INIT,
 *     If the file failed to operate, return RC_WRITE_FAILED.
 **/
RC createPageFile (char *fileName) {
    CHECK_PTR(fileName, RC_FILE_NOT_FOUND);
    if (fileName[0] == 0) { return RC_FILE_NOT_FOUND; }

    FILE *fp = fopen(fileName, "w");
    if (!fp) { return RC_FILE_HANDLE_NOT_INIT; }

    // In default of size in 1 page.
    char buffer[PAGE_SIZE] = "";
    WRITE_OPERATION(fwrite(buffer, sizeof(buffer), 1, fp));

    fclose(fp);
    return RC_OK;
}

/**
 * openPageFile
 *     Opens an existing page file. Should return RC_FILE_NOT_FOUND if the file does not exist.
 *     The second parameter is an existing file handle. If opening the file is successful, then
 *     the fields of this file handle should be initialized with the information about the opened
 *     file. For instance, you would have to read the total number of pages that are stored in the
 *     file from disk.
 *   parameters:
 *     fileName: the filename of the page file.
 *     fHandle: the handle to be created by this function.
 *   return:
 *     If the file does not exist, return RC_FILE_NOT_FOUND
 **/
RC openPageFile (char *fileName, SM_FileHandle *fHandle) {
    CHECK_PTR(fileName, RC_FILE_NOT_FOUND);
    CHECK_FILE_HANDLE(fHandle);

    if (!fHandle) {
        fHandle = (SM_FileHandle*)malloc(sizeof(SM_FileHandle));
    }

    // open file
    FILE *fp = fopen(fileName, "rb+");
    fHandle->mgmtInfo = fp;
    if (!fp) { return RC_FILE_NOT_FOUND; }

    // initialize file handle
    fHandle->fileName = fileName;
    fHandle->curPagePos = 0;

    // get file size and number of pages
    struct stat st;
    if(-1 == stat(fileName, &st)) {
        return RC_FILE_NOT_FOUND;
    }

    off_t fsize = st.st_size;
    if (0 == fsize) { fHandle->totalNumPages = 1; }
    else if (fsize % PAGE_SIZE != 0) { fHandle->totalNumPages = fsize / PAGE_SIZE + 1; }
    else { fHandle->totalNumPages = fsize / PAGE_SIZE; }

    return RC_OK;
}

/**
 * closePageFile
 *     Close an open page file.
 *   parameters:
 *     fHandle: the handle of the file.
 **/
RC closePageFile(SM_FileHandle *fHandle) {
    CHECK_FILE_HANDLE(fHandle)

    FILE *fp = fHandle->mgmtInfo;
    fclose(fp);
    //free(fHandle);
    //fHandle = NULL;
    return RC_OK;
}

/**
 * destroyPageFile
 *     destroy (delete) a page file.
 *   parameters:
 *     fileName: the name of the file to be deleted.
 **/
RC destroyPageFile(char *fileName) {
    CHECK_PTR(fileName, RC_FILE_NOT_FOUND);

    if (-1 == remove(fileName)) {
        return RC_FILE_NOT_FOUND;
    }
    return RC_OK;
}

/**
 * readBlock
 *     The method reads the pageNumth block from a file and stores its content in the memory
 *     pointed to by the memPage page handle. If the file has less than pageNum pages, the
 *     method should return RC_READ_NON_EXISTING_PAGE.
 *   parameters:
 *     pageNum: which page to read in file
 *     fHandle: the file handle
 *     memPage: the memory block
 **/
RC readBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    if (pageNum >= fHandle->totalNumPages || pageNum < 0) { return RC_READ_NON_EXISTING_PAGE; }

    off_t offset = pageNum * PAGE_SIZE; // zero-based index

    FILE *fp = fHandle->mgmtInfo;
    char* buffer = (char*)malloc(PAGE_SIZE);

    if (0 == fseek(fp, offset, SEEK_SET)) {
        if (fread(buffer, PAGE_SIZE, 1, fp) < 1) {
            return RC_READ_NON_EXISTING_PAGE;
        }
    } else {
        return RC_READ_NON_EXISTING_PAGE; // this shouldn't happen
    }

    memcpy(memPage, buffer, PAGE_SIZE);

    return RC_OK;
}

/**
 * getBlockPos
 *     Return the current page position in a file.
 **/
int getBlockPos(SM_FileHandle *fHandle) {
    if (!fHandle) { return -1; } // Don't use CHECK_FILE_HANDLE here! RC_HANDLE_NOT_INIT == 2 !!!
    return fHandle->curPagePos;
}

/**
 * readFirstBlock
 *     Read the first page in a file
 **/
RC readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(0, fHandle, memPage);
}

/**
 * readPreviousBlock
 *     Read the previous page relative to the curPagePos of the file. The curPagePos
 *     should be moved to the page that was read. If the user tries to read a block before the first 
 *     page of after the last page of the file, the method should return RC_READ_NON_EXISTING_PAGE.
 **/
RC readPreviousBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    return readBlock(fHandle->curPagePos - 1, fHandle, memPage);
}

/**
 * readCurrentBlock
 *     Read the current page relative to the curPagePos of the file. The curPagePos
 *     should be moved to the page that was read. If the user tries to read a block before the first
 *     page of after the last page of the file, the method should return RC_READ_NON_EXISTING_PAGE.
 **/
RC readCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

/**
 * readNextBlock
 *     Read the next page relative to the curPagePos of the file. The curPagePos
 *     should be moved to the page that was read. If the user tries to read a block before the first
 *     page of after the last page of the file, the method should return RC_READ_NON_EXISTING_PAGE.
 **/
RC readNextBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    return readBlock(fHandle->curPagePos + 1, fHandle, memPage);
}

/**
 * readLastBlock
 *     Read the last page in a file
 **/
RC readLastBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    return readBlock(fHandle->totalNumPages - 1, fHandle, memPage);
}

/**
 * writeBlock
 *     Write a page to disk using an absolute position.
 **/
RC writeBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);
    if (pageNum < 0) { return RC_WRITE_FAILED; } // can be larger than fHandle->totalNumPages
    if (pageNum >= fHandle->totalNumPages) {
        int result = ensureCapacity(pageNum + 1, fHandle);
        if (RC_OK != result) { return result; }
    }

    off_t offset = pageNum * PAGE_SIZE;

    FILE *fp = fHandle->mgmtInfo;
    WRITE_OPERATION(fseek(fp, offset, SEEK_SET));
    WRITE_OPERATION(fwrite(memPage, PAGE_SIZE, 1, fp));
    if (fflush(fp)) { return RC_WRITE_FAILED; }

    return RC_OK;
}

/**
 * writeCurrentBlock
 *     Write a page to disk using current position.
 **/
RC writeCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    CHECK_FILE_HANDLE(fHandle);

    return writeBlock(fHandle->curPagePos, fHandle, memPage);
}

/**
 * appendEmptyBlock
 * Increase the number of pages in the file by one. The new last page should be filled with zero bytes.
 **/
RC appendEmptyBlock(SM_FileHandle *fHandle) {
    CHECK_FILE_HANDLE(fHandle);

    FILE *fp = fHandle->mgmtInfo;
    WRITE_OPERATION(fseeko(fp, 0, SEEK_END));
    if (ftello(fp) % PAGE_SIZE != 0) { return RC_WRITE_FAILED; }

    char buffer[PAGE_SIZE] = "";
    WRITE_OPERATION(fwrite(buffer, sizeof(buffer), 1, fp));
    fHandle->totalNumPages ++;

    return RC_OK;
}

/**
 * ensureCapacity
 * If the file has less than numberOfPages pages then increase the size to numberOfPages.
 **/
RC ensureCapacity(int numberOfpages, SM_FileHandle *fHandle) {
    CHECK_FILE_HANDLE(fHandle);

    if (fHandle->totalNumPages >= numberOfpages) { return RC_OK; }
    while (fHandle->totalNumPages < numberOfpages) {
        int result = appendEmptyBlock(fHandle);
        if (RC_OK != result) { return result; }
    }

    return RC_OK;
}
