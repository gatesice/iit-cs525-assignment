## IIT CS525 Assignment 3

All functions are realized

### 1. degisn and implement

#### data structure

`Position` the location of each record in the page file , contains page number and slot number

`RM_INFO` manages a table file information, contains a fileHandle and numOfTuples attribute

`scanHandle` deals with scan functions, contains the current position that are ready to scan and the scan condition

#### page structure

Record manager manages many tables and each table is a file on disk. There are many records in each table. For each
table file, the first one or few pages are used to store the schema of this table. And the remainning pages are used 
to store records. At the beginning of first page, storing the number of pages used by the schema. For example, if 
the schema needs 2 pages to store, then at the beginning of first page, the value is 2. Also, in each of remainning 
pages, there can be many records. So in order to deal with inserting and deleting records, we define an index - the 
first available space in this page at the beginning of each page.

#### implementation

We insert records to pages sequentially. When inserting records, first, we need to find the first available slot in 
all pages. If we find it, then insert this record into this space. If not, we create a new page in this file and 
insert this record into the first slot of this page. And then update the index of each page, simply, let the index
points to next available slot.

As for scan functions. In the startScan function, we initialize the start position and scan condition. When scanning,
traversal all pages except pages that store schema, justify every record from page to page until there is no more
tuples. If one record matches the scan condition, then output the result and update scan position.

#### method description

`initRecordManager` initialize the record manager, set its status to open.

`shutdownRecordManager` shut down the record manager, set its status to close.

`createTable` create a page file for a table.

`openTable` open a page file and initialize its instance.

`closeTable` close a table and destroy the handle.

`deleteTable` delete a table file.

`getNumTuples` return the number of tuples in this table.

`insertRecord` insert a new record into the table file. When there is no space for new record, then create a new page. 
And then insert record into the first available slot.

`deleteRecord` delete a record from the table file. Set its data to NULL.

`updateRecord` update the data of the record based on RID.

`getRecord` get the data of the record based on RID.

`startScan` initialize RM_scanHandle, such as start position and scan condition.

`next` get next record that satisfy scan condition. Traversal all pages, if the record matches condition, then output, 
until there are no more tuples.

`closeScan` close the RM_scanHandle.

`getRecordSize` return the size of one record or the schema.

`createSchema` create a schema and set its all attributes.

`freeSchema` release a schema data.

`createRecord` initialize the record such as malloc space for pointers.

`freeRecord` free a record data.

`getAttr` get the value of i-th attribute.

`setAttr` set the value of i-th attribute.

### 2. compile and run

Clone the repository first, then checkout into assignment3 tag.

``` bash
git clone https://gatesice@bitbucket.org/gatesice/iit-cs525-assignment.git assignment3
cd assignment3
git checkout assignment3
```

You can checkout into `assignment1` or `assignment2` tag if you want. Simply replace `assignment3` by `assignment1` or `assignment2` in above commands.

Compile was done by Makefile, this will compile all the documents that are need and link those together. In the terminal, under the assignment folder.

Use the following command to compile the file:

``` bash
make ass3_1
make ass3_2
```
ass3_1 is a bin file for test_assign3_1.c

ass3_2 is a bin file for test_expr.c


To clean all compiled bin files, use the following command:

``` bash
make clean
```

To run the binary file, use the following command:

``` bash
bin/ass3_1
bin/ass3_2
```
