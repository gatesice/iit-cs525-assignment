#ifndef BUFFER_STRATEGY_CLOCK_H
#define BUFFER_STRATEGY_CLOCK_H

#include "../buffer_mgr.h"
#include "../buffer_mgr_ex.h"
#include "../storage_mgr.h"

#include "../dt.h"
#include "../dberror.h"

/**
 * CLOCK_PageNode
 *
 * Used for storing PageHandler.
 * The size of the link list is the same as the numPages in the BufferPool.
 * If one page has no loadded page, the ph will be NULL.
 **/
typedef struct CLOCK_PageNode {
    BM_PageHandle *ph;
    bool isDirty;
    int fixCount; // use this as count of nodes in head node.
} CLOCK_PageNode;

typedef struct CLOCK_PageGlobal {
    int nextInsert;
} CLOCK_PageGlobal;

RC CLOCK_init(BM_BufferPool * const bm);
RC CLOCK_destroy(BM_BufferPool * const bm);
RC CLOCK_flush(BM_BufferPool * const bm);

BM_PageHandle *CLOCK_request(BM_BufferPool * const bm, const int pageNum);
RC CLOCK_write(BM_BufferPool * const bm, BM_PageHandle * const page);

RC CLOCK_makeDirty(BM_BufferPool * const bm, const int pageNum);
RC CLOCK_resetDirty(BM_BufferPool * const bm, const int pageNum);
RC CLOCK_pin(BM_BufferPool * const bm, const int pageNum);
RC CLOCK_unpin(BM_BufferPool * const bm, const int pageNum);

int *CLOCK_getFixCounts(BM_BufferPool * const bm);
bool *CLOCK_getDirtyFlags(BM_BufferPool * const bm);
PageNumber *CLOCK_getFrameContents(BM_BufferPool * const bm);

#endif

