#include <stdlib.h>
#include <string.h>

#include "LRU.h"

/*
 * LRU_INIT
 *
 * pageHandleNode is a pointer to an array that stores LRU page information
 * pageHandleGlobal stores the current index of inserting
 * */
RC LRU_init(BM_BufferPool * const bm) {
    ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode =
            (LRU_PageNode *) calloc(bm->numPages, sizeof(LRU_PageNode));
    ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleGlobal =
            (LRU_PageGlobal *) calloc(1, sizeof(LRU_PageGlobal));
    return RC_OK;
}

RC LRU_destroy(BM_BufferPool * const bm) {
    LRU_PageNode *nodes = ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].fixCount > 0) {
            return RC_BM_BUFFER_IN_USE;
        }
    }

    SM_FileHandle fHandle;
    RC _rc;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty) {
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
        }

        free(nodes[i].ph->data);
        free(nodes[i].ph);
    }

    closePageFile(&fHandle);

    return RC_OK;
}

RC LRU_flush(BM_BufferPool * const bm) {
    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;

    SM_FileHandle fHandle;
    RC _rc;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty && nodes[i].fixCount == 0) {
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
            nodes[i].isDirty = false;
        }
    }

    _rc = closePageFile(&fHandle);
    CHECKEX(_rc);

    return RC_OK;
}

/*
 * findByPageNumber
 *
 * return the index of nodes that has the pageNum
 * */
int LRU_findByPageNumber(BM_BufferPool * const bm, const int pageNum) {
    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages && nodes[i].ph; i++) {
        if (nodes[i].ph->pageNum == pageNum) {
            return i;
        }
    }

    return -1;
}

BM_PageHandle *LRU_request(BM_BufferPool * const bm, const int pageNum) {

    int index = LRU_findByPageNumber(bm, pageNum);

    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;
    LRU_PageGlobal *g =
            (LRU_PageGlobal *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleGlobal;

    //index >=0 indicates that page is already in buffer pool

    int minIndex = g->minIndex;

    if (index >= 0) {
        g->order[g->count] = index;
        if (g->count < bm->numPages)
            g->count++;
        if (g->count == bm->numPages) {
            g->count = bm->numPages - 1;
            g->minIndex = g->order[0];
            int i;
            for (i = 1; i < bm->numPages; i++)
                g->order[i - 1] = g->order[i];
        }
        return nodes[index].ph;
    }

    //otherwise, read it from disk
    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECK_OR_NULL(_rc);
    _rc = ensureCapacity(pageNum + 1, &fHandle);
    CHECK_OR_NULL(_rc);

    do {
        //if nodes[minINdex].pu==NULL, write data to this node
        if (nodes[minIndex].ph == NULL) {
            nodes[minIndex].ph = (BM_PageHandle *) calloc(1,
                    sizeof(BM_PageHandle));
            nodes[minIndex].ph->pageNum = pageNum;
            nodes[minIndex].ph->data = (char *) calloc(1, PAGE_SIZE);

            _rc = readBlock(pageNum, &fHandle, nodes[minIndex].ph->data);
            CHECK_OR_NULL(_rc);
            countReadIO(bm);

            closePageFile(&fHandle);

            g->order[g->count] = minIndex;
            if (g->count < bm->numPages) {
                g->count++;
                g->minIndex = (g->minIndex + 1) % bm->numPages;
            }
            if (g->count == bm->numPages) {
                g->count = bm->numPages - 1;
                g->minIndex = g->order[0];
                int i;
                for (i = 1; i < bm->numPages; i++)
                    g->order[i - 1] = g->order[i];
            }

            return nodes[minIndex].ph;
        }
        //if fixCount==0, then if isDirty, we need write data to disk, then read its content from disk to buffer
        else if (nodes[minIndex].fixCount == 0) {
            if (nodes[minIndex].isDirty) {
                _rc = writeBlock(minIndex, &fHandle, nodes[minIndex].ph->data);
                CHECK_OR_NULL(_rc);
                countWriteIO(bm);
            }

            nodes[minIndex].ph->pageNum = pageNum;
            _rc = readBlock(pageNum, &fHandle, nodes[minIndex].ph->data);
            CHECK_OR_NULL(_rc);
            countReadIO(bm);

            closePageFile(&fHandle);

            g->order[g->count] = minIndex;
            if (g->count < bm->numPages) {
                g->count++;
                g->minIndex = (g->minIndex + 1) % bm->numPages;
            }
            if (g->count == bm->numPages) {
                g->count = bm->numPages - 1;
                g->minIndex = g->order[0];
                int i;
                for (i = 1; i < bm->numPages; i++)
                    g->order[i - 1] = g->order[i];
            }

            return nodes[minIndex].ph;
        }
        //otherwise
        g->order[g->count] = minIndex;
        if (g->count < bm->numPages) {
            g->count++;
            g->minIndex = (g->minIndex + 1) % bm->numPages;
        }
        if (g->count == bm->numPages) {
            g->count = bm->numPages - 1;
            g->minIndex = g->order[0];
            int i;
            for (i = 1; i < bm->numPages; i++)
                g->order[i - 1] = g->order[i];
        }

    } while (minIndex != g->minIndex);

    return NULL;
}

RC LRU_write(BM_BufferPool * const bm, BM_PageHandle * const page) {
    int index = LRU_findByPageNumber(bm, page->pageNum);

    if (index == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    LRU_PageNode *node =
            &(((LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode)[index]);

    memcpy(node->ph->data, page->data, PAGE_SIZE);
    return RC_OK;
}

RC LRU_makeDirty(BM_BufferPool * const bm, const int pageNum) {
    int index = LRU_findByPageNumber(bm, pageNum);

    if (index == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    LRU_PageNode *node =
            &(((LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode)[index]);

    node->isDirty = true;
    return RC_OK;
}

RC LRU_resetDirty(BM_BufferPool * const bm, const int pageNum) {
    int index = LRU_findByPageNumber(bm, pageNum);

    if (index == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    LRU_PageNode *node =
            &(((LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode)[index]);

    node->isDirty = false;
    return RC_OK;
}

RC LRU_pin(BM_BufferPool * const bm, const int pageNum) {
    int index = LRU_findByPageNumber(bm, pageNum);

    if (index == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    LRU_PageNode *node =
            &(((LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode)[index]);

    node->fixCount++;
    return RC_OK;
}

RC LRU_unpin(BM_BufferPool * const bm, const int pageNum) {
    int index = LRU_findByPageNumber(bm, pageNum);

    if (index == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    LRU_PageNode *node =
            &(((LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode)[index]);

    node->fixCount--;
    if (node->fixCount < 0) {
        node->fixCount = 0;
    }

    return RC_OK;
}

int *LRU_getFixCounts(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;
    int *r = (int *) malloc(sizeof(int) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].fixCount;
        } else {
            r[i] = 0;
        }
    }

    return r;
}

bool *LRU_getDirtyFlags(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;
    bool *r = (bool *) malloc(sizeof(bool) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].isDirty;
        } else {
            r[i] = false;
        }
    }

    return r;
}

PageNumber *LRU_getFrameContents(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    LRU_PageNode *nodes =
            (LRU_PageNode *) ((BM_BufferPoolExtra *) bm->mgmtData)->pageHandleNode;
    PageNumber *r = (PageNumber *) malloc(sizeof(PageNumber) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].ph->pageNum;
        } else {
            r[i] = NO_PAGE;
        }
    }

    return r;
}
