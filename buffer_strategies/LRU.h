#ifndef BUFFER_STRATEGY_LRU_H
#define BUFFER_STRATEGY_LRU_H

#include "../buffer_mgr.h"
#include "../buffer_mgr_ex.h"
#include "../storage_mgr.h"

#include "../dt.h"
#include "../dberror.h"

typedef struct LRU_PageNode {
	BM_PageHandle *ph;
	bool isDirty;
	int fixCount;
} LRU_PageNode;

typedef struct LRU_PageGlobal {
	int minIndex;
	int order[100];
	int count;
} LRU_PageGlobal;

RC LRU_init(BM_BufferPool * const bm);
RC LRU_destroy(BM_BufferPool * const bm);
RC LRU_flush(BM_BufferPool * const bm);

BM_PageHandle *LRU_request(BM_BufferPool * const bm, const int pageNum);
RC LRU_write(BM_BufferPool * const bm, BM_PageHandle * const page);

RC LRU_makeDirty(BM_BufferPool * const bm, const int pageNum);
RC LRU_resetDirty(BM_BufferPool * const bm, const int pageNum);
RC LRU_pin(BM_BufferPool * const bm, const int pageNum);
RC LRU_unpin(BM_BufferPool * const bm, const int pageNum);

int *LRU_getFixCounts(BM_BufferPool * const bm);
bool *LRU_getDirtyFlags(BM_BufferPool * const bm);
PageNumber *LRU_getFrameContents(BM_BufferPool * const bm);

#endif

