#include <stdlib.h>
#include <string.h>

#include "CLOCK.h"

int CLOCK_findByPageNumber(BM_BufferPool * const bm, const int pageNum) {
    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages && nodes[i].ph; i++) {
        if (nodes[i].ph->pageNum == pageNum) {
            return i;
        }
    }

    return -1;
}

/**
 * CLOCK_PageNode *CLOCK_init()
 *
 * Initialize a CLOCK strategy for PageHandler.
 * Returns a pointer of PageNode with size of 2
 **/
RC CLOCK_init(BM_BufferPool * const bm) {
    ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode = (CLOCK_PageNode *) calloc(bm->numPages,
            sizeof(CLOCK_PageNode));
    ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleGlobal = (CLOCK_PageGlobal *) calloc(1,
            sizeof(CLOCK_PageGlobal));
    return RC_OK;
}

RC CLOCK_destroy(BM_BufferPool * const bm) {
    // Ensure no pinned page here.
    CLOCK_PageNode *nodes = ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].fixCount > 0) {
            return RC_BM_BUFFER_IN_USE;
        }
    }

    // Destroy the list
    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    // Write dirty pages if any
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty) {
            // Write page to file
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
        }

        free(nodes[i].ph->data);
        free(nodes[i].ph);
    }

    closePageFile(&fHandle);

    return RC_OK;
}

RC CLOCK_flush(BM_BufferPool * const bm) {
    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty && nodes[i].fixCount == 0) {
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
            nodes[i].isDirty = false;
        }
    }

    _rc = closePageFile(&fHandle);
    CHECKEX(_rc);

    return RC_OK;
}

/**
 * CLOCK_request()
 *
 * request a page
 * if the page exists in buffer, return the page contents.
 * otherwise, load the data from the file and then return the contents.
 **/
BM_PageHandle *CLOCK_request(BM_BufferPool * const bm, const int pageNum) {
    int indx = CLOCK_findByPageNumber(bm, pageNum);

    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    CLOCK_PageGlobal *g = (CLOCK_PageGlobal *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleGlobal;

    RC _rc;

    if (indx >= 0) {
        return nodes[indx].ph;
    }

    SM_FileHandle fHandle;
    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECK_OR_NULL(_rc);
    _rc = ensureCapacity(pageNum + 1, &fHandle);
    CHECK_OR_NULL(_rc);

    int __nextInsertStart = g->nextInsert;

    do {
        if (nodes[__nextInsertStart].ph == NULL) {
            // Insert a new PageHandle to non-full list.
            nodes[__nextInsertStart].ph = (BM_PageHandle *) calloc(1,
                    sizeof(BM_PageHandle));
            nodes[__nextInsertStart].ph->pageNum = pageNum;
            nodes[__nextInsertStart].ph->data = (char *) calloc(1, PAGE_SIZE);
            _rc = readBlock(pageNum, &fHandle, nodes[__nextInsertStart].ph->data);
            CHECK_OR_NULL(_rc);
            countReadIO(bm);

            closePageFile(&fHandle);

            g->nextInsert = (__nextInsertStart + 1) % bm->numPages;
            return nodes[__nextInsertStart].ph;
        } else if (nodes[__nextInsertStart].fixCount == 0) {
            // will be covered by new page

            if (nodes[__nextInsertStart].isDirty) {
                // Open file and write the page to disk.
                _rc = writeBlock(nodes[__nextInsertStart].ph->pageNum, &fHandle,
                        nodes[__nextInsertStart].ph->data);
                CHECK_OR_NULL(_rc);
                countWriteIO(bm);
            }

            // Overwrite page
            nodes[__nextInsertStart].ph->pageNum = pageNum;
            _rc = readBlock(pageNum, &fHandle, nodes[__nextInsertStart].ph->data);
            CHECK_OR_NULL(_rc);
            countReadIO(bm);

            closePageFile(&fHandle);

            g->nextInsert = (__nextInsertStart + 1) % bm->numPages;
            return nodes[__nextInsertStart].ph;
        }

        __nextInsertStart = (__nextInsertStart + 1) % bm->numPages;
    } while (__nextInsertStart != g->nextInsert);

    return NULL;
}

RC CLOCK_write(BM_BufferPool * const bm, BM_PageHandle * const page) {
    int indx = CLOCK_findByPageNumber(bm, page->pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    CLOCK_PageNode *node = &(((CLOCK_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    if (node->isDirty) {
        memcpy(node->ph->data, page->data, PAGE_SIZE);
    }
    return RC_OK;
}

RC CLOCK_makeDirty(BM_BufferPool * const bm, const int pageNum) {
    int indx = CLOCK_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    CLOCK_PageNode *node = &(((CLOCK_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->isDirty = true;
    return RC_OK;
}

RC CLOCK_resetDirty(BM_BufferPool * const bm, const int pageNum) {
    int indx = CLOCK_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    CLOCK_PageNode *node = &(((CLOCK_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->isDirty = false;
    return RC_OK;
}

RC CLOCK_pin(BM_BufferPool * const bm, const int pageNum) {
    int indx = CLOCK_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    CLOCK_PageNode *node = &(((CLOCK_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->fixCount++;
    return RC_OK;
}

RC CLOCK_unpin(BM_BufferPool * const bm, const int pageNum) {
    int indx = CLOCK_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    CLOCK_PageNode *node = &(((CLOCK_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->fixCount--;
    if (node->fixCount < 0) {
        node->fixCount = 0;
    }

    return RC_OK;
}

int *CLOCK_getFixCounts(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    int *r = (int *) malloc(sizeof(int) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].fixCount;
        } else {
            r[i] = 0;
        }
    }

    return r;
}

bool *CLOCK_getDirtyFlags(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    bool *r = (bool *) malloc(sizeof(bool) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].isDirty;
        } else {
            r[i] = false;
        }
    }

    return r;
}

PageNumber *CLOCK_getFrameContents(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    CLOCK_PageNode *nodes = (CLOCK_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    PageNumber *r = (PageNumber *) malloc(sizeof(PageNumber) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].ph->pageNum;
        } else {
            r[i] = NO_PAGE;
        }
    }

    return r;

}
