#include <stdlib.h>
#include <string.h>

#include "FIFO.h"

int FIFO_findByPageNumber(BM_BufferPool * const bm, const int pageNum) {
    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages && nodes[i].ph; i++) {
        if (nodes[i].ph->pageNum == pageNum) {
            return i;
        }
    }

    return -1;
}

int FIFO_findEarliestNonPinnedPage(BM_BufferPool * const bm) {
    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    int i, min = -1, minOrder = -1;
    for (i = 0; i < bm->numPages; i ++) {
        if (nodes[i].ph == NULL) {
            return i;
        }

        if (nodes[i].fixCount == 0) {
            if (min == -1 || nodes[i].order < minOrder) {
                min = i;
                minOrder = nodes[i].order;
            }
        }
    }

    return min;
}

/**
 * FIFO_PageNode *FIFO_init()
 *
 * Initialize a FIFO strategy for PageHandler.
 * Returns a pointer of PageNode with size of 2
 **/
RC FIFO_init(BM_BufferPool * const bm) {
    ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode = (FIFO_PageNode *) calloc(bm->numPages,
            sizeof(FIFO_PageNode));
    ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleGlobal = (FIFO_PageGlobal *) calloc(1,
            sizeof(FIFO_PageGlobal));
    return RC_OK;
}

RC FIFO_destroy(BM_BufferPool * const bm) {
    // Ensure no pinned page here.
    FIFO_PageNode *nodes = ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].fixCount > 0) {
            return RC_BM_BUFFER_IN_USE;
        }
    }

    // Destroy the list
    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    // Write dirty pages if any
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty) {
            // Write page to file
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
        }
        free(nodes[i].ph->data);
        free(nodes[i].ph);
    }

    closePageFile(&fHandle);

    return RC_OK;
}

RC FIFO_flush(BM_BufferPool * const bm) {
    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;

    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].isDirty && nodes[i].fixCount == 0) {
            _rc = writeBlock(nodes[i].ph->pageNum, &fHandle, nodes[i].ph->data);
            CHECKEX(_rc);
            countWriteIO(bm);
            nodes[i].isDirty = false;
        }
    }

    _rc = closePageFile(&fHandle);
    CHECKEX(_rc);

    return RC_OK;
}

/**
 * FIFO_request()
 *
 * request a page
 * if the page exists in buffer, return the page contents.
 * otherwise, load the data from the file and then return the contents.
 **/
BM_PageHandle *FIFO_request(BM_BufferPool * const bm, const int pageNum) {
    int indx = FIFO_findByPageNumber(bm, pageNum);

    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    FIFO_PageGlobal *g = (FIFO_PageGlobal *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleGlobal;

    RC _rc;

    if (indx >= 0) {
        return nodes[indx].ph;
    }

    SM_FileHandle fHandle;
    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECK_OR_NULL(_rc);
    _rc = ensureCapacity(pageNum + 1, &fHandle);
    CHECK_OR_NULL(_rc);

    int __min = FIFO_findEarliestNonPinnedPage(bm);

    if (__min == -1) {
        return NULL;
    }

    if (nodes[__min].ph == NULL) {
        // Insert a new PageHandle to non-full list.
        nodes[__min].ph = (BM_PageHandle *) calloc(1,
                sizeof(BM_PageHandle));
        nodes[__min].ph->pageNum = pageNum;
        nodes[__min].ph->data = (char *) calloc(1, PAGE_SIZE);
        nodes[__min].order = g->curr;
        g->curr ++;
        _rc = readBlock(pageNum, &fHandle, nodes[__min].ph->data);
        CHECK_OR_NULL(_rc);
        countReadIO(bm);

        closePageFile(&fHandle);

        return nodes[__min].ph;
    } else if (nodes[__min].fixCount == 0) {
        // will be covered by new page

        if (nodes[__min].isDirty) {
            // Open file and write the page to disk.
            _rc = writeBlock(nodes[__min].ph->pageNum, &fHandle,
                    nodes[__min].ph->data);
            CHECK_OR_NULL(_rc);
            countWriteIO(bm);
        }

        // Overwrite page
        nodes[__min].ph->pageNum = pageNum;
        nodes[__min].isDirty = false;
        nodes[__min].order = g->curr;
        g->curr ++;
        _rc = readBlock(pageNum, &fHandle, nodes[__min].ph->data);
        CHECK_OR_NULL(_rc);
        countReadIO(bm);

        closePageFile(&fHandle);

        return nodes[__min].ph;
    }

    return NULL;
}

RC FIFO_write(BM_BufferPool * const bm, BM_PageHandle * const page) {
    int indx = FIFO_findByPageNumber(bm, page->pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    FIFO_PageNode *node = &(((FIFO_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    if (node->isDirty) {
        memcpy(node->ph->data, page->data, PAGE_SIZE);
    }
    return RC_OK;
}

RC FIFO_makeDirty(BM_BufferPool * const bm, const int pageNum) {
    int indx = FIFO_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    FIFO_PageNode *node = &(((FIFO_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->isDirty = true;
    return RC_OK;
}

RC FIFO_resetDirty(BM_BufferPool * const bm, const int pageNum) {
    int indx = FIFO_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    FIFO_PageNode *node = &(((FIFO_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->isDirty = false;
    return RC_OK;
}

RC FIFO_pin(BM_BufferPool * const bm, const int pageNum) {
    int indx = FIFO_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    FIFO_PageNode *node = &(((FIFO_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->fixCount++;
    return RC_OK;
}

RC FIFO_unpin(BM_BufferPool * const bm, const int pageNum) {
    int indx = FIFO_findByPageNumber(bm, pageNum);

    if (indx == -1) {
        return RC_BM_PAGE_NOT_BUFFERED;
    }

    FIFO_PageNode *node = &(((FIFO_PageNode *)((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode)[indx]);

    node->fixCount--;
    if (node->fixCount < 0) {
        node->fixCount = 0;
    }

    return RC_OK;
}

int *FIFO_getFixCounts(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    int *r = (int *) malloc(sizeof(int) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].fixCount;
        } else {
            r[i] = 0;
        }
    }

    return r;
}

bool *FIFO_getDirtyFlags(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    bool *r = (bool *) malloc(sizeof(bool) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].isDirty;
        } else {
            r[i] = false;
        }
    }

    return r;
}

PageNumber *FIFO_getFrameContents(BM_BufferPool * const bm) {
    if (!bm) {
        return NULL;
    }

    FIFO_PageNode *nodes = (FIFO_PageNode *) ((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode;
    PageNumber *r = (PageNumber *) malloc(sizeof(PageNumber) * bm->numPages);

    int i;
    for (i = 0; i < bm->numPages; i++) {
        if (nodes[i].ph) {
            r[i] = nodes[i].ph->pageNum;
        } else {
            r[i] = NO_PAGE;
        }
    }

    return r;

}
