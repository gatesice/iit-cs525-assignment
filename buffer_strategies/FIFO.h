#ifndef BUFFER_STRATEGY_FIFO_H
#define BUFFER_STRATEGY_FIFO_H

#include "../buffer_mgr.h"
#include "../buffer_mgr_ex.h"
#include "../storage_mgr.h"

#include "../dt.h"
#include "../dberror.h"

/**
 * FIFO_PageNode
 *
 * Used for storing PageHandler.
 * The size of the link list is the same as the numPages in the BufferPool.
 * If one page has no loadded page, the ph will be NULL.
 **/
typedef struct FIFO_PageNode {
    BM_PageHandle *ph;
    bool isDirty;
    int fixCount; // use this as count of nodes in head node.
    int order;
} FIFO_PageNode;

typedef struct FIFO_PageGlobal {
    int curr;
} FIFO_PageGlobal;

RC FIFO_init(BM_BufferPool * const bm);
RC FIFO_destroy(BM_BufferPool * const bm);
RC FIFO_flush(BM_BufferPool * const bm);

BM_PageHandle *FIFO_request(BM_BufferPool * const bm, const int pageNum);
RC FIFO_write(BM_BufferPool * const bm, BM_PageHandle * const page);

RC FIFO_makeDirty(BM_BufferPool * const bm, const int pageNum);
RC FIFO_resetDirty(BM_BufferPool * const bm, const int pageNum);
RC FIFO_pin(BM_BufferPool * const bm, const int pageNum);
RC FIFO_unpin(BM_BufferPool * const bm, const int pageNum);

int *FIFO_getFixCounts(BM_BufferPool * const bm);
bool *FIFO_getDirtyFlags(BM_BufferPool * const bm);
PageNumber *FIFO_getFrameContents(BM_BufferPool * const bm);

#endif

