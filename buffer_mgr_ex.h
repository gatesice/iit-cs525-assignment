#ifndef BUFFER_MANAGER_EXTRA_H
#define BUFFER_MANAGER_EXTRA_H

#include "buffer_mgr.h"
#include "storage_mgr.h"

#include "dt.h"
#include "dberror.h"

#define CHECKEX(__expression__) if (__expression__ != RC_OK) { return __expression__; }
#define CHECK_OR_NULL(__expression__) if (__expression__ != RC_OK) { return NULL; }

// Stores extra info for BufferPool
typedef struct BM_BufferPoolExtra {
    int readIO;
    int writeIO;

    void *pageHandleNode;
    void *pageHandleGlobal;
} BM_BufferPoolExtra;

// Buffer Pool Managerment
RC initBufferPoolExtra(BM_BufferPool *const bm, void *stratData);
RC shutdownBufferPoolExtra(BM_BufferPool *const bm);

RC countReadIO(BM_BufferPool *const bm);
RC countWriteIO(BM_BufferPool *const bm);
#endif

