CC = gcc
CFLAGS = -g -Wall
MKDIR = mkdir -p

default: ass3_1

## assignment 1

ass1: foldercreation test_assign1_1.o storage_mgr.o dberror.o 
	$(CC) $(CFLAGS) -o bin/ass1 build/test_assign1_1.o build/storage_mgr.o build/dberror.o

test_assign1_1.o: 
	$(CC) $(CFLAGS) -o build/test_assign1_1.o -c test_assign1_1.c

storage_mgr.o: 
	$(CC) $(CFLAGS) -o build/storage_mgr.o -c storage_mgr.c

dberror.o: 
	$(CC) $(CFLAGS) -o build/dberror.o -c dberror.c

## assignment 2

ass2: foldercreation test_assign2_1.o storage_mgr.o dberror.o buffer_mgr.o buffer_mgr_ex.o buffer_mgr_stat.o FIFO.o LRU.o CLOCK.o
	$(CC) $(CFLAGS) -o bin/ass2 build/test_assign2_1.o build/storage_mgr.o build/dberror.o build/buffer_mgr.o build/buffer_mgr_stat.o build/buffer_mgr_ex.o build/buffer_strategy_FIFO.o build/buffer_strategy_LRU.o build/buffer_strategy_CLOCK.o

test_assign2_1.o:
	$(CC) $(CFLAGS) -o build/test_assign2_1.o -c test_assign2_1.c

buffer_mgr.o:
	$(CC) $(CFLAGS) -o build/buffer_mgr.o -c buffer_mgr.c

buffer_mgr_ex.o:
	$(CC) $(CFLAGS) -o build/buffer_mgr_ex.o -c buffer_mgr_ex.c

buffer_mgr_stat.o:
	$(CC) $(CFLAGS) -o build/buffer_mgr_stat.o -c buffer_mgr_stat.c

FIFO.o:
	$(CC) $(CFLAGS) -o build/buffer_strategy_FIFO.o -c buffer_strategies/FIFO.c

LRU.o:
	$(CC) $(CFLAGS) -o build/buffer_strategy_LRU.o -c buffer_strategies/LRU.c

CLOCK.o:
	$(CC) $(CFLAGS) -o build/buffer_strategy_CLOCK.o -c buffer_strategies/CLOCK.c

## assignment 2 extra

ass2_ex: foldercreation test_assign2_2.o storage_mgr.o dberror.o buffer_mgr.o buffer_mgr_ex.o buffer_mgr_stat.o FIFO.o LRU.o CLOCK.o
	$(CC) $(CFLAGS) -o bin/ass2ex build/test_assign2_2.o build/storage_mgr.o build/dberror.o build/buffer_mgr.o build/buffer_mgr_stat.o build/buffer_mgr_ex.o build/buffer_strategy_FIFO.o build/buffer_strategy_LRU.o build/buffer_strategy_CLOCK.o

test_assign2_2.o:
	$(CC) $(CFLAGS) -o build/test_assign2_2.o -c test_assign2_2.c


## assignment 3

ass3_1: foldercreation test_assign3_1.o storage_mgr.o dberror.o buffer_mgr.o buffer_mgr_ex.o buffer_mgr_stat.o FIFO.o LRU.o CLOCK.o expr.o rm_serializer.o record_mgr.o
	$(CC) $(CFLAGS) -o bin/ass3_1 build/test_assign3_1.o build/storage_mgr.o build/dberror.o build/buffer_mgr.o build/buffer_mgr_stat.o build/buffer_mgr_ex.o build/buffer_strategy_FIFO.o build/buffer_strategy_LRU.o build/buffer_strategy_CLOCK.o build/expr.o build/rm_serializer.o build/record_mgr.o

ass3_2: foldercreation test_expr.o storage_mgr.o dberror.o buffer_mgr.o buffer_mgr_ex.o buffer_mgr_stat.o FIFO.o LRU.o CLOCK.o expr.o rm_serializer.o record_mgr.o
	$(CC) $(CFLAGS) -o bin/ass3_2 build/test_expr.o build/storage_mgr.o build/dberror.o build/buffer_mgr.o build/buffer_mgr_stat.o build/buffer_mgr_ex.o build/buffer_strategy_FIFO.o build/buffer_strategy_LRU.o build/buffer_strategy_CLOCK.o build/expr.o build/rm_serializer.o build/record_mgr.o

test_assign3_1.o:
	$(CC) $(CFLAGS) -o build/test_assign3_1.o -c test_assign3_1.c

test_expr.o:
	$(CC) $(CFLAGS) -o build/test_expr.o -c test_expr.c

expr.o:
	$(CC) $(CFLAGS) -o build/expr.o -c expr.c

rm_serializer.o:
	$(CC) $(CFLAGS) -o build/rm_serializer.o -c rm_serializer.c

record_mgr.o:
	$(CC) $(CFLAGS) -o build/record_mgr.o -c record_mgr.c

foldercreation:
	$(MKDIR) bin
	$(MKDIR) build

clean:
	rm -r bin
	rm -r build

