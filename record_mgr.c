// ============================================================================
//
//   record_mgr_tables.c
//
//   by Gates_ice
//
// =============================================================================

# include <string.h>
# include <stdlib.h>
# include "record_mgr.h"
# include "storage_mgr.h"
# include "buffer_mgr.h"

/**
 * Position stores the position of one page.
 *
 **/
typedef struct position {
    int page_id;
    int slot_id;
} Position;

/**
 * RM_INFO manages a table information, contains a fileHandle and numOfTuples attribute.
 *
 **/
typedef struct RM_INFO {
    SM_FileHandle fh;
    int numOfTuples;
} RM_INFO;

/**
 * scanHandle
 *
 * page and slot are the current location that are ready to scan
 * cond is the scan condition
 *
 **/

typedef struct scanHandle {
    int page;
    int slot;
    Expr *cond;
} scanHandle;

// ========================================
// Global Variables
// ========================================

bool MGR_OPENED = false;
void *MGMT_DATA = NULL;
SM_FileHandle fHandle;
int numOfTuples = 0;

// ========================================
// Util functions in this part
// ========================================

#define MEMCPY_TO_OFFSET(__expression__, __type__) memcpy(&(buffer[offset]), __expression__, sizeof(__type__)); offset += sizeof(__type__)

#define MEMCPY_FROM_OFFSET(__expression__, __type__) memcpy(__expression__, &(buffer[offset]), sizeof(__type__)); offset += sizeof(__type__)

/**
 * writeSchemaData()
 *
 * Write schema data into the file.
 *
 **/
RC dumpSchemaData(Schema *schema, SM_FileHandle *fHandle) {
    int sizeSchema = sizeof(int) * (3 + schema->numAttr * 3 + schema->keySize)
            + sizeof(DataType) * schema->numAttr;
    int attrNameOffset = sizeSchema;
    int offset = 0;

    int i;
    for (i = 0; i < schema->numAttr; i++) { //  calculate total size that attribute names taken
        sizeSchema += strlen(schema->attrNames[i]) + 1; //  including the NULL @ the end
    }

    //  create the buffer
    int numPages = (sizeSchema - 1) / PAGE_SIZE + 1; // numPages is the total number of pages used for schema
    char *buffer = (char *) malloc(PAGE_SIZE * numPages);

    MEMCPY_TO_OFFSET(&numPages, int);

    MEMCPY_TO_OFFSET(&(schema->numAttr), int);
    MEMCPY_TO_OFFSET(&(schema->keySize), int);

    for (i = 0; i < schema->numAttr; i++) {
        int slen = strlen(schema->attrNames[i]) + 1;
        MEMCPY_TO_OFFSET(&attrNameOffset, int);
        MEMCPY_TO_OFFSET(&slen, int);
        MEMCPY_TO_OFFSET(&(schema->dataTypes[i]), DataType);
        MEMCPY_TO_OFFSET(&(schema->typeLength[i]), int);

        memcpy(&(buffer[attrNameOffset]), schema->attrNames[i], slen);
        attrNameOffset += slen;
    }

    for (i = 0; i < schema->keySize; i++) {
        MEMCPY_TO_OFFSET(&(schema->keyAttrs[i]), int);
    }

    //  write to file
    ensureCapacity(numPages, fHandle);
    RC _rc;
    for (i = 0; i < numPages; i++) {
        _rc = writeBlock(i, fHandle,
                (SM_PageHandle) (&(buffer[i * PAGE_SIZE])));
        if (_rc != RC_OK) {
            return _rc;
        }
    }

    return RC_OK;
}

/**
 * loadSchemaData()
 *
 * Load the schema data from the file.
 *
 **/
Schema *loadSchemaData(SM_FileHandle *fHandle) {
    Schema *schema = (Schema *) malloc(sizeof(Schema));

    //  load the first page to get num pages
    char *buffer = (char *) malloc(PAGE_SIZE);
    RC _rc = readBlock(0, fHandle, (SM_PageHandle) buffer);
    if (_rc != RC_OK) {
        return NULL;
    }
    int numPages = 0;
    memcpy(&numPages, buffer, sizeof(int));

    int i;
    free(buffer);
    buffer = (char *) malloc(PAGE_SIZE * numPages);
    //  read schema from page file into buffer
    for (i = 0; i < numPages; i++) {
        _rc = readBlock(i, fHandle, (SM_PageHandle) (&buffer[i * PAGE_SIZE]));
        if (_rc != RC_OK) {
            return NULL;
        }
    }

    //  get data from buffer
    int offset = sizeof(int);
    MEMCPY_FROM_OFFSET(&schema->numAttr, int);
    MEMCPY_FROM_OFFSET(&schema->keySize, int);

    schema->attrNames = (char **) malloc(sizeof(char *) * schema->numAttr);
    schema->dataTypes = (DataType *) malloc(sizeof(DataType) * schema->numAttr);
    schema->typeLength = (int *) malloc(sizeof(int *) * schema->numAttr);
    schema->keyAttrs = (int *) malloc(sizeof(int *) * schema->keySize);

    int attrNameOffset, slen;
    for (i = 0; i < schema->numAttr; i++) {
        MEMCPY_FROM_OFFSET(&attrNameOffset, int);
        MEMCPY_FROM_OFFSET(&slen, int);
        MEMCPY_FROM_OFFSET(&schema->dataTypes[i], DataType);
        MEMCPY_FROM_OFFSET(&schema->typeLength[i], int);

        schema->attrNames[i] = (char *) malloc(slen);
        memcpy(schema->attrNames[i], &buffer[attrNameOffset], slen);
    }

    for (i = 0; i < schema->keySize; i++) {
        MEMCPY_FROM_OFFSET(&schema->keyAttrs[i], int);
    }

    return schema;
}

/**
 * findFirstAvailablePage()
 *
 * Traversal table file and find the first available page position
 *
 **/
Position findFirstAvailablePage(int startPos, SM_FileHandle *fHandle) {
    Position res;
    int i = startPos;

    for (; i <= fHandle->totalNumPages; i++) { //   traversal all pages from the start page
        int slot;
        fseek(fHandle->mgmtInfo, i * PAGE_SIZE, SEEK_SET);
        fread(&slot, sizeof(int), 1, fHandle->mgmtInfo);

        if (slot != -1) { //    slot == -1 means there are not available slots in this page
            res.page_id = i;
            res.slot_id = slot;
            return res;
        }
    }

    res.page_id = -1; // pagre_id == -1 means there are not available slots in all pages in the file

    return res;
}

// ========================================
// Table functions from header
// ========================================

/**
 * initRecordManager()
 *
 * Initialize a record manager.
 *
 **/
RC initRecordManager(void *mgmtData) {
    if (!MGR_OPENED) {
        MGR_OPENED = true;
        MGMT_DATA = mgmtData;
    }

    return RC_OK;
}

/**
 * shutdownRecordManager()
 *
 * Shutdown a record manager.
 *
 **/
RC shutdownRecordManager() {
    MGR_OPENED = false;
    MGMT_DATA = NULL;

    return RC_OK;
}

/**
 * createTable()
 *
 * Create and initialize a table file.
 *
 **/
RC createTable(char *name, Schema *schema) {
    //  validate
    if (!schema) {
        return RC_NULL_POINTER;
    }

    if (!MGR_OPENED) {
        return RC_RM_MANAGER_CLOSED;
    }

    if (openPageFile(name, &fHandle) != RC_FILE_NOT_FOUND) {
        closePageFile(&fHandle);
        return RC_RM_TABLE_EXISTS;
    }

    //  create the table
    RC _rc = createPageFile(name);
    if (_rc != RC_OK) {
        return _rc;
    }
    _rc = openPageFile(name, &fHandle);
    if (_rc != RC_OK) {
        return _rc;
    }

    _rc = dumpSchemaData(schema, &fHandle);
    if (_rc != RC_OK) {
        return _rc;
    }

    _rc = closePageFile(&fHandle);
    if (_rc != RC_OK) {
        return _rc;
    }

    return RC_OK;
}

/**
 * openTable()
 *
 * Open a table, and initialize the handle.
 *
 **/
RC openTable(RM_TableData *rel, char *name) {
    //  validate
    if (!rel || !name) {
        return RC_NULL_POINTER;
    }

    //  open the table and load the schema data.
    RC _rc = openPageFile(name, &fHandle);
    if (_rc != RC_OK) {
        return _rc;
    }

    Schema *schema = loadSchemaData(&fHandle);
    if (!schema) {
        return RC_RM_INVALID_SCHEMA_DATA;
    }

    //  set rel name, schema and mgmtData
    RM_INFO *ri = (RM_INFO *) malloc(sizeof(RM_INFO));
    ri->fh = fHandle;
    ri->numOfTuples = numOfTuples;

    rel->name = name;
    rel->schema = schema;
    rel->mgmtData = ri;

    return RC_OK;
}

/**
 * closeTable()
 *
 * Close a table and destroy the handle.
 *
 **/
RC closeTable(RM_TableData *rel) {
    //  validate
    if (!rel)
        return RC_NULL_POINTER;
    /*  can't use free here because rel are not allocated with malloc, calloc or realloc
     freeSchema(rel->schema);
     if (rel->mgmtData) {
     free(rel->mgmtData);
     }
     free(rel);
     */

    return closePageFile(&fHandle);
}

/**
 * deleteTable()
 *
 * Delete a table file.
 *
 **/
RC deleteTable(char *name) {
    //  validate
    if (!name) {
        return RC_OK;  //   let it go
    }

    destroyPageFile(name);  //  let it go if not exists

    return RC_OK;
}

/**
 * getNumTuples()
 *
 * Get number of tuples in the table.
 *
 **/
int getNumTuples(RM_TableData *rel) {
    return ((RM_INFO *) rel->mgmtData)->numOfTuples;
}

/**
 * insertRecord()
 *
 * insert an record into page file
 *
 **/
RC insertRecord(RM_TableData *rel, Record *record) {
    //  validate
    if (!rel || !record)
        return RC_NULL_POINTER;

    //  load the first page to get num pages
    char *buffer = (char *) malloc(PAGE_SIZE);
    RC _rc = readBlock(0, &fHandle, (SM_PageHandle) buffer);

    if (_rc != RC_OK) {
        return _rc;
    }
    int numPages = 0;
    memcpy(&numPages, buffer, sizeof(int));

    Schema *schema = rel->schema;
    Position pos;

    if (fHandle.totalNumPages == numPages) { // there is no page for records, so generating a new page
        ensureCapacity(1, &fHandle);
        (&fHandle)->totalNumPages++;

        int nextAvailableSlot = 0;
        if (fseek(fHandle.mgmtInfo, numPages * PAGE_SIZE, SEEK_SET) != 0)
            return RC_WRITE_FAILED;
        if (fwrite(&nextAvailableSlot, sizeof(int), 1, fHandle.mgmtInfo) == -1)
            return RC_WRITE_FAILED;
        //  update pos
        pos.page_id = numPages;
        pos.slot_id = 0;
    } else if (fHandle.totalNumPages > numPages) {  //  traversal pages
        pos = findFirstAvailablePage(numPages, &fHandle);

        if (pos.page_id == -1) {    //  can't find available slot from all pages
            //printf("create a new page\n");
            ensureCapacity(1, &fHandle);
            (&fHandle)->totalNumPages++;

            int nextAvailableSlot = 0;
            if (fseek(fHandle.mgmtInfo, (fHandle.totalNumPages - 1) * PAGE_SIZE,
            SEEK_SET) != 0)
                return RC_WRITE_FAILED;
            if (fwrite(&nextAvailableSlot, sizeof(int), 1, fHandle.mgmtInfo)
                    == -1)
                return RC_WRITE_FAILED;
            //  set pos
            pos.page_id = fHandle.totalNumPages - 1;
            pos.slot_id = 0;
        }
    }

    //  get record data based on pos
    int schemaSize = getRecordSize(schema);
    int offset = pos.page_id * PAGE_SIZE + pos.slot_id * schemaSize
            + sizeof(int);
    if (fseek(fHandle.mgmtInfo, offset, SEEK_SET) != 0)
        return RC_WRITE_FAILED;
    if (fwrite(record->data, schemaSize, 1, fHandle.mgmtInfo) == -1)
        return RC_WRITE_FAILED;
    //  set record page and slot
    record->id.page = pos.page_id;
    record->id.slot = pos.slot_id;
    //printf("%d %d\n",pos.page_id, pos.slot_id);

    //update next available slot in each page
    if (fseek(fHandle.mgmtInfo, pos.page_id * PAGE_SIZE, SEEK_SET) != 0)
        return RC_WRITE_FAILED;
    int nextPos = pos.slot_id + 1;
    if ((nextPos + 1) * schemaSize + sizeof(int) > PAGE_SIZE)
        nextPos = -1;
    if (fwrite(&nextPos, sizeof(int), 1, fHandle.mgmtInfo) == -1)
        return RC_WRITE_FAILED;

    numOfTuples++;
    ((RM_INFO *) rel->mgmtData)->numOfTuples++;
    return RC_OK;
}

/**
 * deleteRecord()
 *
 * delete a record from a table file
 *
 **/
RC deleteRecord(RM_TableData *rel, RID id) {
    //  validate
    if (!rel)
        return RC_NULL_POINTER;

    //  set the data of record with id as NULL
    int page_id = id.page;
    int slot_id = id.slot;
    Schema *schema = rel->schema;
    int schemaSize = getRecordSize(schema);
    int offset = page_id * PAGE_SIZE + slot_id * schemaSize + sizeof(int);

    if (fseek(fHandle.mgmtInfo, offset, SEEK_SET) != 0)
        return RC_WRITE_FAILED;
    char *data = (char *) malloc(sizeof(char) * schemaSize);
    if (fwrite(data, schemaSize, 1, fHandle.mgmtInfo) == -1)
        return RC_WRITE_FAILED;

    numOfTuples--;
    ((RM_INFO *) rel->mgmtData)->numOfTuples--;
    return RC_OK;
}

/**
 * updateRecord()
 *
 * update record data
 *
 * */
RC updateRecord(RM_TableData *rel, Record *record) {
    //  validate
    if (!rel)
        return RC_NULL_POINTER;

    //  update the data of record with id
    int page_id = record->id.page;
    int slot_id = record->id.slot;
    Schema *schema = rel->schema;
    int schemaSize = getRecordSize(schema);
    int offset = page_id * PAGE_SIZE + sizeof(int) + slot_id * schemaSize;

    if (fseek(fHandle.mgmtInfo, offset, SEEK_SET) != 0)
        return RC_WRITE_FAILED;
    if (fwrite(record->data, schemaSize, 1, fHandle.mgmtInfo) == 0)
        return RC_WRITE_FAILED;

    return RC_OK;
}

/**
 * getRecord()
 *
 * get the data of record
 *
 * */
RC getRecord(RM_TableData *rel, RID id, Record *record) {
    //  validate
    if (!rel || !record)
        return RC_NULL_POINTER;

    int page_id = id.page;
    int slot_id = id.slot;
    int schemaSize = getRecordSize(rel->schema);
    int offset = page_id * PAGE_SIZE + sizeof(int) + slot_id * schemaSize;

    //  store record data into record->data
    if (fseek(fHandle.mgmtInfo, offset, SEEK_SET) != 0)
        return RC_WRITE_FAILED;
    if (fread(record->data, schemaSize, 1, fHandle.mgmtInfo) < 1)
        return RC_READ_NON_EXISTING_PAGE;

    return RC_OK;
}

/**
 * startScan()
 *
 * initialize RM_scanHandle
 *
 **/
RC startScan(RM_TableData *rel, RM_ScanHandle *scan, Expr *cond) {
    //  validate
    if (!rel || !scan || !cond)
        return RC_NULL_POINTER;

    //  load the first page to get num pages
    char *buffer = (char *) malloc(PAGE_SIZE);
    RC _rc = readBlock(0, &fHandle, (SM_PageHandle) buffer);

    if (_rc != RC_OK) {
        return _rc;
    }
    int numPages = 0;
    memcpy(&numPages, buffer, sizeof(int));

    //  initialize scanHandle and RM_ScanHandle
    scanHandle *sh = (scanHandle *) malloc(sizeof(scanHandle));
    sh->page = numPages;
    sh->slot = 0;
    sh->cond = cond;

    scan->rel = rel;
    scan->mgmtData = sh;

    return RC_OK;
}

/**
 * next()
 *
 * get next record that satisfy scan condition
 *
 **/
RC next(RM_ScanHandle *scan, Record *record) {
    //  validate
    if (!scan || !record)
        return RC_NULL_POINTER;

    Record *re = (Record *) malloc(sizeof(Record)); // re is the temporary record
    re->data = (char *) malloc(sizeof(char));
    RID rid;
    Value *value;

    RM_TableData *rel = (RM_TableData *) malloc(sizeof(RM_TableData));
    scanHandle *sh = (scanHandle *) malloc(sizeof(scanHandle));
    rel = scan->rel;
    sh = (scanHandle *) scan->mgmtData;

    //  page and slot are the current position of scan
    int page = sh->page;
    int slot = sh->slot;
    int schemaSize = getRecordSize(rel->schema);

    if (!sh->cond) {    //  if cond is NULL, then any records can be retrieved
        int offset = page * PAGE_SIZE + sizeof(int) + slot * schemaSize;
        if (offset > fHandle.totalNumPages * PAGE_SIZE) {   //no more tuples
            return RC_RM_NO_MORE_TUPLES;
        }
        if (fread(record->data, sizeof(schemaSize), 1, fHandle.mgmtInfo) < 1)
            return RC_READ_NON_EXISTING_PAGE;

        //  update slot. If this is the last slot in this page, then page++ and slot=0
        slot++;
        if (sizeof(int) + (slot + 1) * schemaSize > PAGE_SIZE) {
            page++;
            slot = 0;
        }
        sh->page = page;
        sh->slot = slot;
        scan->mgmtData = sh;
    } else {    //  cond is not NULL
        while (1) {     //  break while loop until there are no more tuples
            int offset = page * PAGE_SIZE + sizeof(int) + slot * schemaSize;
            if (offset > fHandle.totalNumPages * PAGE_SIZE) {
                return RC_RM_NO_MORE_TUPLES;
            }

            //  set rid
            rid.page = page;
            rid.slot = slot;
            //  get record data from table and store result to re
            getRecord(rel, rid, re);
            //  compare whether re matches the scan condition and store result to value
            evalExpr(re, rel->schema, sh->cond, &value);

            if (value->v.boolV) {   //  re matches the scan condition
                *record = *re;

                //  update RM_ScanHandle
                slot++;
                if (sizeof(int) + (slot + 1) * schemaSize > PAGE_SIZE) {
                    page++;
                    slot = 0;
                }
                sh->page = page;
                sh->slot = slot;
                scan->mgmtData = sh;

                break;
            }
            //  don't match, then update RM_ScanHandle, and continue
            slot++;
            if (sizeof(int) + (slot + 1) * schemaSize > PAGE_SIZE) {
                page++;
                slot = 0;
            }
            sh->page = page;
            sh->slot = slot;
            scan->mgmtData = sh;
        }
    }   //  end of else

    return RC_OK;
}

/**
 * closeScan()
 *
 * close the scan
 *
 **/
RC closeScan(RM_ScanHandle *scan) {
    /*  can't use free here because scan are not allocated with malloc, calloc or realloc
     scanHandle *sh = (scanHandle *)scan->mgmtData;
     free(scan->rel);
     free(sh->cond);
     free(sh);

     free(scan);
     */

    return RC_OK;
}

/**
 * createRecord()
 *
 * initialize the record
 *
 **/
RC createRecord(Record **record, Schema *schema) {
    //  validate
    if (!record || !schema)
        return RC_NULL_POINTER;

    Record *r = (Record *) malloc(sizeof(Record));
    char *data = (char *) malloc(sizeof(char) * getRecordSize(schema));

    r->data = data;
    *(record) = r;

    return RC_OK;
}

/**
 * freeRecord()
 *
 * free a record
 *
 * */
RC freeRecord(Record *record) {
    //  validate
    if (!record)
        return RC_NULL_POINTER;

    free(record->data);

    free(record);

    return RC_OK;
}

/**
 * getAttr()
 *
 * get the value of one attribute
 *
 * */
RC getAttr(Record *record, Schema *schema, int attrNum, Value **value) {
    //  validate
    if (!record)
        return RC_NULL_POINTER;

    Value *v = (Value *) malloc(sizeof(Value));
    int i = 0, offset = 0;
    while (i < attrNum) {   //  get the offset of attribute
        if (schema->dataTypes[i] == DT_STRING)
            offset += (schema->typeLength[i]) * sizeof(char);
        else
            offset += sizeof(schema->dataTypes[i]);
        i++;
    }

    switch (schema->dataTypes[attrNum]) {   //  get the value of this attribute
    case DT_INT:
        memcpy(&(v->v.intV), &(record->data[offset]), sizeof(int));
        v->dt = DT_INT;
        break;
    case DT_STRING:
        v->v.stringV = malloc((schema->typeLength[i]) * sizeof(char));
        memcpy(v->v.stringV, &(record->data[offset]),
                (schema->typeLength[i]) * sizeof(char));
        v->dt = DT_STRING;
        break;
    case DT_FLOAT:
        memcpy(&(v->v.floatV), &(record->data[offset]), sizeof(float));
        v->dt = DT_FLOAT;
        break;
    case DT_BOOL:
        memcpy(&(v->v.boolV), &(record->data[offset]), sizeof(bool));
        v->dt = DT_BOOL;
        break;
    }

    (*value) = v;

    return RC_OK;
}

/**
 * setAttr()
 *
 * set the attribute value
 *
 * */
RC setAttr(Record *record, Schema *schema, int attrNum, Value *value) {
    //  validate
    if (!record || !schema)
        return RC_NULL_POINTER;

    int i = 0, offset = 0;
    while (i < attrNum) {   //  get the offset of attribute
        offset += sizeof(schema->dataTypes[i]);
        i++;
    }

    switch (value->dt) {    //  set value
    case DT_INT:
        memcpy(&(record->data[offset]), &value->v.intV, sizeof(int));
        break;
    case DT_STRING:
        memcpy(&(record->data[offset]), value->v.stringV,
                strlen(value->v.stringV));
        break;
    case DT_FLOAT:
        memcpy(&(record->data[offset]), &value->v.floatV, sizeof(float));
        break;
    case DT_BOOL:
        memcpy(&(record->data[offset]), &value->v.boolV, sizeof(bool));
        break;
    }

    return RC_OK;
}

/**
 * getRecordSize()
 *
 * get the size of one record or schema
 *
 * */
int getRecordSize(Schema *schema) {
    //  validate
    if (!schema) {
        return 0;
    }

    int i, size = 0;
    for (i = 0; i < schema->numAttr; i++) {
        switch (schema->dataTypes[i]) {
        case DT_INT:
            size += sizeof(int);
            break;
        case DT_STRING:
            size += schema->typeLength[i];
            break;
        case DT_FLOAT:
            size += sizeof(float);
            break;
        case DT_BOOL:
            size += sizeof(bool);
            break;
        default:
            break;
        }
    }

    return size;
}

/**
 * createSchema()
 *
 * Create a schema.
 *
 **/
Schema *createSchema(int numAttr, char **attrNames, DataType *dataTypes,
        int *typeLength, int keySize, int *keys) {
    if (numAttr <= 0) {
        return NULL;
    }

    Schema *schema = (Schema *) malloc(sizeof(Schema));

    schema->numAttr = numAttr;
    schema->keySize = keySize;

    schema->attrNames = (char **) malloc(sizeof(char *) * numAttr);
    schema->dataTypes = (DataType *) malloc(sizeof(DataType) * numAttr);
    schema->typeLength = (int *) malloc(sizeof(int) * numAttr);
    schema->keyAttrs = (int *) malloc(sizeof(int) * keySize);

    int i;
    for (i = 0; i < numAttr; i++) {
        schema->attrNames[i] = attrNames[i];
        schema->dataTypes[i] = dataTypes[i];
        schema->typeLength[i] = typeLength[i];
    }

    for (i = 0; i < keySize; i++) {
        schema->keyAttrs[i] = keys[i];
    }

    return schema;
}

/**
 * freeSchema()
 *
 * Release a schema data.
 *
 **/
RC freeSchema(Schema *schema) {
    if (!schema) {
        return RC_OK;  // let it go
    }

    free(schema->attrNames);
    free(schema->dataTypes);
    free(schema->typeLength);
    free(schema->keyAttrs);

    free(schema);

    return RC_OK;
}
