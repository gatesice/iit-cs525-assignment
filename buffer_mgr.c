#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "buffer_mgr.h"
#include "buffer_mgr_ex.h"
#include "storage_mgr.h"

#include "buffer_strategies/FIFO.h"
#include "buffer_strategies/LRU.h"
#include "buffer_strategies/CLOCK.h"
#include "buffer_strategies/LFU.h"
#include "buffer_strategies/LRU_K.h"

// Buffer Pool Functions

/**
 * RC initBufferPool
 * 
 * creates a new buffer pool with numPages page frames using the page replacement 
 * strategy strategy. The pool is used to cache pages from the page file with name 
 * pageFileName. Initially, all page frames should be empty. The page file should 
 * already exist, i.e., this method should not generate a new page file. stratData 
 * can be used to pass parameters for the page replacement strategy. For example, 
 * for LRU-k this could be the parameter k.
 **/
RC initBufferPool (BM_BufferPool *const bm,
        const char *const pageFileName,
        const int numPages,
        ReplacementStrategy strategy,
        void *stratData) {

    if (!bm) {
        return RC_NULL_POINTER;
    }

    if (!pageFileName) {
        return RC_FILE_NOT_FOUND;
    }

    RC _rc;

    bm->pageFile = (char*)pageFileName; //it is not a good idea
    bm->numPages = numPages;
    bm->strategy = strategy;
    _rc = initBufferPoolExtra(bm, stratData);
    CHECKEX(_rc);
    
    if (!bm->mgmtData) {
        return RC_NULL_POINTER;
    }

    return RC_OK;
}

/**
 * RC shutdownBufferPool(BM_BufferPool *const bm)
 *
 * destroys a buffer pool. This method should free up all resources associated with 
 * buffer pool. For example, it should free the memory allocated for page frames. 
 * If the buffer pool contains any dirty pages, then these pages should be written 
 * back to disk before destroying the pool. It is an error to shutdown a buffer 
 * pool that has pinned pages.
 **/
RC shutdownBufferPool (BM_BufferPool *const bm) {
        if (!bm) {
            return RC_NULL_POINTER;
        }

        RC _rc = shutdownBufferPoolExtra(bm);
        CHECKEX(_rc);

        bm->pageFile = NULL;

        return RC_OK;
}

/**
 * RC forceFlushPool
 *
 * causes all dirty pages (with fix count 0) from the buffer pool to be written 
 * to disk.
 **/
RC forceFlushPool (BM_BufferPool *const bm) {
    if (!bm) {
        return RC_NULL_POINTER;
    }

    RC _rc;

    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_flush(bm);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_flush(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_flush(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_flush(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_flush(bm);
            CHECKEX(_rc);
            break;
#endif

        default:
            return RC_BM_INVALID_STRATEGY;
    }
    
    return RC_OK;
        

}


// Buffer Manager Interface Access Pages

/**
 * RC markDirty
 *
 * marks a page as dirty.
 **/
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page) {
    if (!bm || !page) {
        return RC_NULL_POINTER;
    }

    RC _rc;

    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_makeDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_makeDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_makeDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_makeDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_makeDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

        default:
            return RC_BM_INVALID_STRATEGY;
    }
    
    return RC_OK;
}

/**
 * RC unpinPage
 *
 * unpins the page page. The pageNum field of page should be used to figure out 
 * which page to unpin.
 **/
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page) {
    if (!bm || !page) {
        return RC_NULL_POINTER;
    }

    RC _rc;

    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_unpin(bm, page->pageNum);
            CHECKEX(_rc);
            _rc = FIFO_write(bm, page);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_unpin(bm, page->pageNum);
            CHECKEX(_rc);
            _rc = LRU_write(bm, page);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_unpin(bm, page->pageNum);
            CHECKEX(_rc);
            _rc = CLOCK_write(bm, page);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_unpin(bm, page->pageNum);
            CHECKEX(_rc);
            _rc = LFU_write(bm, page);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_unpin(bm, page->pageNum);
            CHECKEX(_rc);
            _rc = FRU_K_write(bm, page);
            CHECKEX(_rc);
            break;
#endif

        default:
            return RC_BM_INVALID_STRATEGY;
    }
    
    return RC_OK;
}

/**
 * RC forcePage
 *
 * should write the current content of the page back to the page file on disk.
 **/
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page) {
    if (!bm || !page) {
        return RC_NULL_POINTER;
    }

    RC _rc;
    SM_FileHandle fHandle;

    _rc = openPageFile(bm->pageFile, &fHandle);
    CHECKEX(_rc);

    _rc = writeBlock(page->pageNum, &fHandle, page->data);
    CHECKEX(_rc);

    closePageFile(&fHandle);

    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_resetDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_resetDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_resetDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_resetDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_resetDirty(bm, page->pageNum);
            CHECKEX(_rc);
            break;
#endif

        default:
            break;
    }

    return RC_OK;
}

/**
 * RC pinPage
 *
 * pins the page with page number pageNum. The buffer manager is responsible to 
 * set the pageNum field of the page handle passed to the method. Similarly, the 
 * data field should point to the page frame the page is stored in (the area in 
 * memory storing the content of the page).
 **/
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
        const PageNumber pageNum) {
    if (!bm || !page) {
        return RC_NULL_POINTER;
    }

    RC _rc;

    BM_PageHandle *p = NULL;

    switch (bm->strategy) {
#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            p = LRU_request(bm, pageNum);
            _rc = LRU_pin(bm, pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            p = CLOCK_request(bm, pageNum);
            _rc = CLOCK_pin(bm, pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            p = LFU_request(bm, pageNum);
            _rc = LFU_pin(bm, pageNum);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            p = LRU_K_request(bm, pageNum);
            _rc = LRU_K_pin(bm, pageNum);
            CHECKEX(_rc);
            break;
#endif

        case RS_FIFO:
        default:
            p = FIFO_request(bm, pageNum);
            _rc = FIFO_pin(bm, pageNum);
            CHECKEX(_rc);
            break;
    }

    if (!p) {
        return RC_NULL_POINTER;
    }

    page->pageNum = pageNum;
    if (!page->data) {
        free(page->data);
    }
    page->data = (char *) malloc(PAGE_SIZE);
    memcpy(page->data, p->data, PAGE_SIZE);

    return RC_OK;

}

// Statistics Interface

/**
 * PageNumber *getFrameContents
 *
 * returns an array of PageNumbers (of size numPages) where the ith element is the 
 * number of the page stored in the ith page frame. An empty page frame is represented 
 * using the constant NO_PAGE.
 **/
PageNumber *getFrameContents (BM_BufferPool *const bm) {
    if (!bm) {
        return NULL;
    }

    switch (bm->strategy) {
        case RS_FIFO:
            return FIFO_getFrameContents(bm);

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            return LRU_getFrameContents(bm);
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            return CLOCK_getFrameContents(bm);
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            return LFU_getFrameContents(bm);
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            return LRU_K_getFrameContents(bm);
#endif

        default:
            break;
    }

    return NULL;
}

/**
 * bool *getDirtyFlags
 *
 * returns an array of bools (of size numPages) where the ith element is TRUE if 
 * the page stored in the ith page frame is dirty. Empty page frames are considered 
 * as clean.
 **/
bool *getDirtyFlags (BM_BufferPool *const bm) {
    if (!bm) {
        return NULL;
    }

    switch (bm->strategy) {
        case RS_FIFO:
            return FIFO_getDirtyFlags(bm);

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            return LRU_getDirtyFlags(bm);
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            return CLOCK_getDirtyFlags(bm);
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            return LFU_getDirtyFlags(bm);
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            return LRU_K_getDirtyFlags(bm);
#endif

        default:
            break;
    }

    return NULL;
}

/**
 * int *getFixCounts
 *
 * returns an array of ints (of size numPages) where the ith element is the fix 
 * count of the page stored in the ith page frame. Return 0 for empty page frames.
 **/
int *getFixCounts (BM_BufferPool *const bm) {
    if (!bm) {
        return NULL;
    }

    switch (bm->strategy) {
        case RS_FIFO:
            return FIFO_getFixCounts(bm);

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            return LRU_getFixCounts(bm);
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            return CLOCK_getFixCounts(bm);
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            return LFU_getFixCounts(bm);
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            return LRU_K_getFixCounts(bm);
#endif

        default:
            break;
    }

    return NULL;
}

/**
 * int getNumReadIO
 *
 * returns the number of pages that have been read from disk since a buffer pool 
 * has been initialized. You code is responsible to initializing this statistic 
 * at pool creating time and update whenever a page is read from the page file into 
 * a page frame.
 **/
int getNumReadIO (BM_BufferPool *const bm) {
    if (!bm || !bm->mgmtData) {
        return -1;
    }

    return ((BM_BufferPoolExtra *)bm->mgmtData)->readIO;
}

/**
 * int getNumWriteIO
 *
 * returns the number of pages written to the page file since the buffer pool has 
 * been initialized.
 **/
int getNumWriteIO (BM_BufferPool *const bm) {
    if (!bm || !bm->mgmtData) {
        return -1;
    }

    return ((BM_BufferPoolExtra *)bm->mgmtData)->writeIO;
}

