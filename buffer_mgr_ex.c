#include <stdlib.h>

#include "buffer_mgr_ex.h"

#include "buffer_strategies/FIFO.h"
#include "buffer_strategies/LRU.h"
#include "buffer_strategies/CLOCK.h"
#include "buffer_strategies/LFU.h"
#include "buffer_strategies/LRU_K.h"

// Buffer Pool Management

/**
 * initBufferPoolExtra()
 *
 * Initialize a new BM_BufferPoolExtra and add it into the list.
 **/
RC initBufferPoolExtra(BM_BufferPool *const bm, void *stratData) {
    BM_BufferPoolExtra *bmx = (BM_BufferPoolExtra *)calloc(1, sizeof(BM_BufferPoolExtra));
    
    bm->mgmtData = bmx;

    // Init the pageHandleNode and pageHandleGlobal
    RC _rc;
    
    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_init(bm);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_init(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_init(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_init(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_init(bm);
            CHECKEX(_rc);
            break;
#endif

        default:
            return RC_BM_INVALID_STRATEGY;
    }
    
    return RC_OK;
}

RC shutdownBufferPoolExtra(BM_BufferPool *const bm) {
    // remove all page handle in buffer pool
    RC _rc;

    switch (bm->strategy) {
        case RS_FIFO:
            _rc = FIFO_destroy(bm);
            CHECKEX(_rc);
            break;

#ifdef BUFFER_STRATEGY_LRU_H
        case RS_LRU:
            _rc = LRU_destroy(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_CLOCK_H
        case RS_CLOCK:
            _rc = CLOCK_destroy(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LFU_H
        case RS_LFU:
            _rc = LFU_destroy(bm);
            CHECKEX(_rc);
            break;
#endif

#ifdef BUFFER_STRATEGY_LRU_K_H
        case RS_LRU_K:
            _rc = LRU_K_destroy(bm);
            CHECKEX(_rc);
            break;
#endif

        default:
            return RC_BM_INVALID_STRATEGY;
    }

    free(((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleNode);
    free(((BM_BufferPoolExtra *)bm->mgmtData)->pageHandleGlobal);
    free(bm->mgmtData);

    return RC_OK;
}


RC countReadIO(BM_BufferPool *const bm) {
    if (!bm) {
        return RC_NULL_POINTER;
    }

    ((BM_BufferPoolExtra *)bm->mgmtData)->readIO ++;
    return RC_OK;
}

RC countWriteIO(BM_BufferPool *const bm) {
    if (!bm) {
        return RC_NULL_POINTER;
    }

    ((BM_BufferPoolExtra *)bm->mgmtData)->writeIO ++;
    return RC_OK;
}


